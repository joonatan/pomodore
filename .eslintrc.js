module.exports = {
  root: true,
  extends: [
    'eslint-config-prettier',
    'eslint-config-airbnb',
    '@react-native-community',
  ],
  ignorePatterns: ['lib/', 'node_modules/'],
  rules: {
    semi: ['error', 'never'],
    quotes: ['error', 'single'],
    'arrow-parens': ['error', 'as-needed'],
    'no-shadow': ['error', {'hoist': 'all'}],
    'prettier/prettier': 0,
    'react/jsx-props-no-spreading': 'error',
    'react/static-property-placement': 'error',
    // TODO enable this
    'react/jsx-filename-extension': 'off',
    'react/jsx-max-props-per-line': [1, { maximum: 1, when: 'multiline' }],
    // have to disable since this doesn't work "<>Foo {value}</>"
    //      otherwise I'll end up writing code <>{'Foo' + value}</>
    'react/jsx-one-expression-per-line': ['off', { allow: 'single-child' }],
    'object-curly-newline': ['error', { multiline: true, minProperties: 5 }],
  },
};
