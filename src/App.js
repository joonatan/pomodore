/** Joonatan Kuosa
 *  2020-03-03
 *  Pomadore App
 *
 * @format
 * @flow
 */

import React from 'react'
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Vibration,
} from 'react-native'
import { Overlay } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'
import Sound from 'react-native-sound'
import { AnimatedCircularProgress } from 'react-native-circular-progress'

// TODO add tests
//      fails currently and has zero covarage
//
// TODO add configurable time (AsyncStorage)
//  use a modal window for it (lower corner button)
//
// TODO add a script to auto copy sounds
//
// TODO add background colour to the view (blue for rest, red for work)
//      related; add separate colour schemes (for buttons, progress bar)
//
// TODO break this into two parts
//      the App part
//      the Time display widget part
//      reuse the time Widget twice one for Rest and one for Work
const MAX_TIME = 10
const MAX_REST_TIME = 5

type Tick = { type: 'Tick' }
type Stop = { type: 'Stop' }
type Pause = { type: 'Pause' }
type Start = { type: 'Start' }
type Done = { type: 'Done' }

type Action = Tick | Stop | Start | Pause | Done

Sound.setCategory('Playback')

type Phase = 'ticking' | 'stopped' | 'ended'
type Round = 'rest' | 'work'

type State = {
  +seconds: number,
  +phase: Phase,
  +round: Round,
};

const isRest = (x: Round): boolean => x === 'rest'
const isEnded = (phase: Phase): boolean => phase === 'ended'
const isTicking = (phase: Phase): boolean => phase === 'ticking'

const initialState : State = { seconds: MAX_TIME, phase: 'stopped', round: 'work' }

const reducer = (state : State, action : Action): State => {
  switch (action.type) {
    case 'Start':
      return { ...state, phase: 'ticking' }
    case 'Stop':
      return { ...state, seconds: MAX_TIME, round: 'work', phase: 'stopped' }
    case 'Pause':
      return { ...state, phase: 'stopped' }
    case 'Done':
      if (isRest(state.round)) {
        return { ...state, round: 'work', phase: 'stopped', seconds: MAX_TIME }
      } else {
        return { ...state, round: 'rest', phase: 'stopped', seconds: MAX_REST_TIME }
      }
    case 'Tick':
      if (isTicking(state.phase) && state.seconds > 0) {
        return { ...state, seconds: state.seconds - 1 }
      } else if (isTicking(state.phase) && state.seconds === 0) {
        return { ...state, phase: 'ended' }
      } else  {
        return state
      }
    default:
      (action: empty)
      return state
  }
}

const fill = (s : number): string => (
  s <= 9 ? `0${s}` : `${s}`
)

const printSeconds = (sec : number) => {
  const h = Math.floor(sec / 60 / 60)
  const m = Math.floor(sec / 60 - h * 60)
  const s = Math.floor(sec % 60)
  // return `${h}:${m}:${s}`
  return h > 0 ? `${fill(h)}:${fill(m)}:${fill(s)}`
    : m > 0 ? `${fill(m)}:${fill(s)}`
      : s
}

// TODO something odd with the prefill
//    using it doesn't start the progress from where it was left
//    we can overload animate and reAnimate to fix?
const TickDisplay = ({ text, progress, color, tintColor }) => {
  const [last, setLast] = React.useState(0)

  return (
    <AnimatedCircularProgress
      size={320}
      width={15}
      fill={progress}
      prefill={last}
      tintColor={tintColor}
      onAnimationComplete={() => setLast(progress)}
      backgroundColor={color}
      dashedBackground={{ width: 5, gap: 5 }}
      duration={0}
    >
      {
        x => (
          <Text style={styles.tomatoDisplay}>{text}</Text>
        )
      }
    </AnimatedCircularProgress>
  )
}

const Btn = ({ onPress, iconName }) => (
  <TouchableOpacity onPress={onPress} style={styles.btnRounded}>
    <Icon style={styles.icon} name={iconName} size={32} color="#000" />
  </TouchableOpacity>
)

const App: () => React$Node = () => {
  const [soundEffect, setSoundEffect] = React.useState(null)
  const [state, dispatch] = React.useReducer(reducer, initialState)

  React.useEffect(() => {
    const sound = new Sound('bleep.mp3', Sound.MAIN_BUNDLE, err => {
      if (err) {
        // TODO display proper error message using toast
        console.error('Sound loading failed: ', err)
      } else {
        setSoundEffect(sound)
      }
    })
  }, [])

  React.useEffect(() => {
    const timerId = setInterval(() => dispatch({ type: 'Tick' }), 1000)
    return (() => clearInterval(timerId))
  })

  React.useEffect(() => {
    if (isEnded(state.phase)) {
      Vibration.vibrate(400)

      if (soundEffect) {
        soundEffect.play()
      }
    }
  }, [state.phase, soundEffect])

  const progress = isRest(state.round)
    ? (1 - (state.seconds / MAX_REST_TIME)) * 100
    : (1 - (state.seconds / MAX_TIME)) * 100

  const color = isRest(state.round) ? '#008' : 'tomato'
  const tintColor = isRest(state.round) ? '#00a' : '#a00'
  return (
    <View style={styles.container}>
      <TickDisplay
        text={printSeconds(state.seconds)}
        progress={progress}
        color={color}
        tintColor={tintColor}
      />
      {( isTicking(state.phase)
        ? (
          <View style={styles.btnContainer}>
            <Btn onPress={_evt => dispatch({ type: 'Pause' })} iconName="pause" />
          </View>
        )
        : (
          <View style={styles.btnContainer}>
            <Btn onPress={_evt => dispatch({ type: 'Start' })} iconName="play" />
            { state.seconds !== MAX_TIME
                && <Btn onPress={_evt => dispatch({ type: 'Stop' })} iconName="refresh" />}
          </View>
        )
      )}
      <Overlay
        isVisible={isEnded(state.phase)}
        windowBackgroundColor="rgba(255, 255, 255, 0.5)"
        overlayBackgroundColor="blue"
        width="auto"
        height="auto"
        onBackdropPress={() => dispatch({ type: 'Done' })}
      >
        <Text>Good job</Text>
      </Overlay>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  btnContainer: { flexDirection: 'row' },
  timeRemaining: { fontSize: 24 },
  tomatoDisplay: { fontSize: 68 },
  icon: { textAlign: 'center' },
  btnRounded : {
    minWidth: 100,
    marginTop:10,
    paddingTop:15,
    paddingBottom:15,
    marginLeft:10,
    marginRight:10,
    backgroundColor:'#faa',
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#a00',
  },
})

export default App
